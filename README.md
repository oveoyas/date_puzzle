# Date puzzle

The notebook in this repository contains code for solving a date puzzle.

The date puzzle has eight pieces and a board with 43 cells, one for each month (Jan-Dec) and day in the month (1-31). The pieces cover 41 cells, leaving two uncovered cells that should correspond to a date (e.g. Mar 22).

You can use the code to solve the puzzle, either by yourself, by randomly placing pieces on the board, or by using the Gurobi optimizer. Using optimization, you can easily find all possible solutions for any given date.
